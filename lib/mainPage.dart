import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './myPage.dart';
import './knowledge.dart';

class MainPage extends StatelessWidget{
  Widget build(BuildContext context){
    return MainScreen();
  }
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => new _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
    int _currentIndex = 1;

    void onTabTapped(int index){
      setState((){
        _currentIndex = index;
      });
    }
    
  @override
  Widget build(BuildContext context) {
    
    void showBottom() {
      showModalBottomSheet<void>(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(30.0),
            topRight: const Radius.circular(30.0),  
          ),
        ),
        builder: (BuildContext context) {
          return Container(
            height: 140.0,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(30.0),
                topRight: const Radius.circular(30.0),
              ),
            ),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AppBar(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(30.0),
                      topRight: const Radius.circular(30.0),  
                    ),
                  ),
                  elevation: 0.0,
                  leading: Container(),
                  actions: [
                    new IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(
                        Icons.clear,
                      ),
                      color: Color.fromARGB(0xFF, 0xFF, 0x8D, 0x88),
                    ),
                  ],
                  title: new Text(
                    '검사지 종류',
                    style: TextStyle(
                      fontFamily: 'Spoqa Han San',
                      fontSize: 14.0,
                      color: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  centerTitle: true,
                  backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
                ),      
                Divider(indent:10, endIndent: 10),
                Container(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Spacer(),
                        ButtonTheme(
                          minWidth: 130.0,
                          height: 43.0,
                          child: new FlatButton(
                              child: new Text(
                                '수술전',
                                style: TextStyle(
                                  fontFamily: 'Spoqa Han San',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13.0,
                                  color: Color.fromARGB(0xFF, 0x3B, 0x3B, 0x4D),
                                ),
                              ),
                              color: Color.fromARGB(0xFF, 0xFF, 0xF0, 0xED),
                              disabledColor:
                                  Color.fromARGB(0xFF, 0xFF, 0xF0, 0xED),
                              onPressed: null,
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(7.0))),
                        ),
                        Spacer(),
                        ButtonTheme(
                            minWidth: 130.0,
                            height: 43.0,
                            child: new FlatButton(
                                child: new Text(
                                  '수술후',
                                  style: TextStyle(
                                    fontFamily: 'Spoqa Han San',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13.0,
                                    color: Color.fromARGB(0xFF, 0x3B, 0x3B, 0x4D),
                                  ),
                                ),
                                color: Color.fromARGB(0xFF, 0xFF, 0xF0, 0xED),
                                disabledColor:
                                    Color.fromARGB(0xFF, 0xFF, 0xF0, 0xED),
                                onPressed: null,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(7.0)))),
                        Spacer(),
                      ],
                    ),
                  )
              ],
            ),
          );
        },
      );
    }
  
    Widget myRecord = Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
        automaticallyImplyLeading: false,
        title: 
          Image(
            image: AssetImage(
              'images/group_13.png'
            ),
            width: 77,
            height: 25,
          ),
        centerTitle: false,
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_none,
            ),
            onPressed: null,
          ),
        ],
      ),
      body:Container(
        color: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            AppBar(
              elevation:0.0,
              backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
              automaticallyImplyLeading: false,
              title: Text(
                '등록한 내 검사기록',
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: 'Spoqa Han San',
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.08,
                  color: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
                ),
                textAlign: TextAlign.left,
              ),
              centerTitle: false,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                  ),
                  Image(
                    image: AssetImage(
                    'images/illustration_1.png'
                    ),
                    width: 220,
                  ),
                  SizedBox(height: 30),
                  Text(
                    '저장된 검사기록이 없습니다.',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Color.fromARGB(0xFF, 0x3B, 0x3B, 0x4D),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    '조직검사지를 사진 찍어 올려주세요!\n'
                    '무슨 내용인지 이해하고 연구하시기 쉽도록,\n'
                    '저희가 중요한 부분을 번역하는 것을 도와드릴게요.\n',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 13.0,
                      color: Color.fromARGB(0xFF, 0x6C, 0x7B, 0x8A),
                    ),
                  ),
                  FlatButton(
                    onPressed: null,
                    child: new Text(
                      '샘플보기>',
                      style: TextStyle(
                        color: Color.fromARGB(0xFF, 0xFF, 0x8D, 0x88),
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: new EdgeInsets.only(right: 20.0),
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(Icons.camera_alt),
                    backgroundColor: Color.fromARGB(0xFF, 0xFF, 0x76, 0x76),
                    onPressed: showBottom,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    final List<Widget> _children = [
      Knowledge(),
      myRecord,
      MyPage(),
    ];
    

    return Scaffold(
      body: _children[_currentIndex], 
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
        selectedFontSize: 10.6,
        unselectedFontSize: 10.6,
        selectedItemColor: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage(
                'images/document.png'
              ),
              color: Color.fromARGB(0xFF, 0x88, 0x88, 0x88),
              width: 20,
            ),
            activeIcon: Image(
              image: AssetImage(
                'images/document.png'
              ),
              width: 20,
              height: 22,
              color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
            ),
            title: Text('유방암 지식'),
          ),
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage(
                'images/edit_2.png'
              ),
              color: Color.fromARGB(0xFF, 0x88, 0x88, 0x88),
              width: 20,
              height: 22,
              ),
            activeIcon: Image(
              image: AssetImage(
                'images/edit_2.png'
              ),
              color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
              width: 20,
              height: 22,  
            ),
            title: Text('내 검사기록'),
          ),
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage(
                'images/combined_shape.png'
              ),
              color: Color.fromARGB(0xFF, 0x88, 0x88, 0x88),
              width: 20,
              height: 22,  
            ),
            activeIcon: Image(
              image: AssetImage(
                'images/combined_shape.png'
              ),
              color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
              width: 20,
              height: 22,  
            ),
            title: Text('마이페이지'),
          ),
        ],
      ),
    );    
  }
}