import 'package:flutter/material.dart';
import './search.dart';
import 'category01.dart';
import 'category02.dart';
import 'category03.dart';

class Knowledge extends StatefulWidget {
  final Widget child;
  Knowledge({Key key, this.child}) : super(key: key);
  @override
  _KnowledgeState createState() => _KnowledgeState();
}

class _KnowledgeState extends State<Knowledge>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  ScrollController _scrollViewController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Text(
                'NABI',
                style: TextStyle(
                  fontSize: 18.0,
                  color: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.search),
                  color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                  onPressed: () => showSearch(
                    context: context,
                    delegate: DataSearch(),
                  ),
                ),
              ],
              pinned: true,
              floating: true,
              forceElevated: boxIsScrolled,
              bottom: TabBar(
                isScrollable: true,
                labelColor: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
                indicatorColor: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                tabs: <Widget>[
                  Tab(text: 'Category01'),
                  Tab(text: 'Category02'),
                  Tab(text: 'Category03'),
                ],
                controller: _tabController,
              ),
            ),
          ];
        },
        body: TabBarView(
          children: <Widget>[
            Category01(),
            Category02(),
            Category03(),
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}