import 'package:flutter/material.dart';

class Category01Tab extends StatefulWidget {
  final Widget child;

  Category01Tab({Key key, this.child}) : super(key: key);

  _Category01TabState createState() => _Category01TabState();
}

class _Category01TabState extends State<Category01Tab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15.0),
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          recentNews(),
          SizedBox(
            height: 10.0,
          ),
          popularNews(),
        ],
      ),
    );
  }

  Widget recentNews() {
    return Material(
      color: Colors.white,
      elevation: 0.0,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: labelContainer('최근 뉴스'),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: recentNewsContents(),
          ),
        ],
      ),
    );
  }

  Widget popularNews() {
    return Material(
      color: Colors.white,
      elevation: 0.0,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: labelContainer('인기 뉴스'),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: hotNewsContents(),
          ),
        ],
      ),
    );
  }

  Widget labelContainer(String labelVal) {
    return Container(
      padding: EdgeInsets.only(right: 15.0),
      height: 20.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            labelVal,
            style: TextStyle(fontSize: 16.3),
          ),
          Text(
            '더보기',
            style: TextStyle(
                color: Color.fromARGB(0xFF, 0x9B, 0x9B, 0x9B), fontSize: 13.0),
          ),
        ],
      ),
    );
  }

  Widget recentImage(String img, String title) {
    return Center(
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 130.0,
              width: 130.0,
              decoration: new BoxDecoration(
                image: DecorationImage(
                  image: new NetworkImage(img),
                  fit: BoxFit.fill,
                ),
                //borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                title,
                style: TextStyle(
                    color: Color.fromARGB(0xFF, 0x26, 0x25, 0x27),
                    fontSize: 11.4),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget hotImage(String img, String title, String authorImg,
      String author, String date, String read) {
    bool isPressed = false;

    return Center(
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 100.0,
              width: 250.0,
              decoration: new BoxDecoration(
                image: DecorationImage(
                  image: new NetworkImage(img),
                  fit: BoxFit.fill,
                ),
                borderRadius: BorderRadius.circular(3.0),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              constraints: BoxConstraints.expand(height: 32, width: 250),
              padding: EdgeInsets.only(left: 11.0, right: 11.0),
              child: Text(
                title,
                style: TextStyle(
                    color: Color.fromARGB(0xFF, 0x26, 0x25, 0x27),
                    fontSize: 11.4),
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 11.0, right: 11.0),
                  child: CircleAvatar(
                    backgroundImage: new NetworkImage(authorImg),
                    minRadius: 10.0,
                    maxRadius: 10.0,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      author,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        color: Color.fromARGB(0xFF, 0xFF, 0x53, 0x38),
                        fontSize: 8.2,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          date,
                          style: TextStyle(
                            color: Color.fromARGB(0xFF, 0x26, 0x25, 0x27),
                            fontSize: 7.3,
                          ),
                        ),
                        Text(
                          read,
                          style: TextStyle(
                            color: Color.fromARGB(0xFF, 0x26, 0x25, 0x27),
                            fontSize: 7.3,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                Container(
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            isPressed = true;
                          });
                        },
                        icon: new Icon(
                          isPressed ? Icons.favorite : Icons.favorite_border,
                          color: isPressed ? Colors.red : null,
                          size: 20.0,
                        ),
                      ),
                    ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget recentNewsContents() {
    return Container(
        height: 170.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            recentImage(
                'https://lh3.googleusercontent.com/48wwD4kfFSStoxwuwCIu6RdM2IeZmZKfb1ZeQkga0qEf1JKsiD-hK3Qf8qvxHL09lQ=s180-rw',
                'News01'),
            SizedBox(
              width: 20.0,
            ),
            recentImage(
                'https://lh3.googleusercontent.com/7uRfJe2KkpKxZuMvY4OjhIq-TJrMeHgWYQt0H7LHZl4WNDAYjI6FFrLSsLhj2g8cqKr5=s180-rw',
                'News02'),
            SizedBox(
              width: 20.0,
            ),
            recentImage(
              'https://lh3.googleusercontent.com/d6TTnyRybU8B2naK8a0y1_u8ufjtK5V-mizS6o1tCx0U1aYPX9nJzcq9rSm5W2VVzBw=s180-rw',
              'News03',
            ),
            SizedBox(
              width: 20.0,
            ),
            recentImage(
              'https://lh3.ggpht.com/-wROmWQVYTcjs3G6H0lYkBK2nPGYsY75Ik2IXTmOO2Oo0SMgbDtnF0eqz-BRR1hRQg=s180-rw',
              'News04',
            ),
          ],
        ));
  }

  Widget hotNewsContents() {
    return Container(
        height: 200.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            hotImage(
              'https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2017/09/dumbbell-circuit.jpg?itok=6s7fDOKM&timestamp=1505300575',
              '"유방암의 90%는 생활습관 때문...체중조절하고 운동하세요"',
              'https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2017/09/dumbbell-circuit.jpg?itok=6s7fDOKM&timestamp=1505300575',
              'Emma Johnson',
              'Nov 26, 2018',
              ' - 3min read',
            ),
            SizedBox(
              width: 20.0,
            ),
            hotImage(
              'https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2017/09/dumbbell-circuit.jpg?itok=6s7fDOKM&timestamp=1505300575',
              '"유방암의 90%는 생활습관 때문...체중조절하고 운동하세요"',
              'https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2017/09/dumbbell-circuit.jpg?itok=6s7fDOKM&timestamp=1505300575',
              'Emma Johnson',
              'Nov 26, 2018',
              ' - 555min read',
            ),
            SizedBox(
              width: 20.0,
            ),
          ],
        ));
  }
}
