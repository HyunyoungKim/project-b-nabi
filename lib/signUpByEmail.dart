import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class SignUpByEmail extends StatelessWidget {  
  Widget build(BuildContext context) {
    return SignUp();
  }
}

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

Future<Register> _registerAccount() async {
  final response =
      await http.get('https://m0qbhopjqf.execute-api.ap-northeast-2.amazonaws.com/api/project-a/biock/register');

  if (response.statusCode == 200) {
    return Register.fromJson(json.decode(response.body));
  }
  else if(response.statusCode != 200){
    return null;
  } 
  else {
    throw Exception('Failed to load post');
  }
} 

class Register{
  String name, email, pw;
  Register({
    this.name,
    this.email,
    this.pw
  });

  factory Register.fromJson(Map<String, dynamic> json){
    return new Register(
      name: json['name'],
      email: json['email'], 
      pw: json['password']
    );
  }
  
  Map toJson(){
    return{'name': name, 'email': email, 'password': pw};
  }
}

class _SignUpState extends State<SignUp> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();
  TextEditingController pwController = new TextEditingController();

  FocusNode nameNode = new FocusNode();
  FocusNode mailNode = new FocusNode();
  FocusNode pwNode = new FocusNode();

  bool isNameSubmitted = false;
  bool isMailSubmitted = false;
  bool isPWSubmitted = false;
  
  bool isNameValid = false;
  bool isMailValid = false;
  bool isPWValid = false;
  
  bool next = false;

  String _name = '';
  String _mail = '';
  String _pw = '';
  
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_backspace,
            color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
            size: 24,
          ),
          onPressed: () {
            FocusScope.of(context).requestFocus(FocusNode());
            Navigator.pop(context);
          },
        ),
        title: Text(
          '이메일로 가입하기',
          style: TextStyle(
            fontSize: 15,
            fontFamily: 'Nanum Gothic',
            letterSpacing: 0.09,
            color: Color.fromARGB(0xFF, 0x33, 0x33, 0x33),
          ),
        ),
        titleSpacing: 0.09,
        centerTitle: true,
        backgroundColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
          alignment: Alignment.centerLeft,
          width: double.infinity,
          margin: const EdgeInsets.only(top: 25, left: 30, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '이름',
                style: TextStyle(
                  fontSize: 11,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0.92,
                  color: Color.fromARGB(0xFF, 0xBE, 0xBE, 0xBE),
                ),
                textAlign: TextAlign.start,
              ),
              TextField(
                style: TextStyle(
                  color: Color.fromARGB(0xFF, 0x26, 0x26, 0x28),
                  fontSize: 16,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0,
                ),
                onChanged: (name) {
                  setState((){
                    _name = name;
                    _nameValid(_name);
                  });
                  _next();
                },
                onSubmitted: (name) {
                  setState((){
                    _name = name;
                    _nameValid(_name);
                  });
                  _next();
                  FocusScope.of(context).requestFocus(nameNode);
                },
                cursorColor: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                controller: nameController,
                maxLines: 1,
                maxLength: 20,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 2,
                    color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                  )),
                ),
              ),
            ],
          ),
        ),
          Container(
          alignment: Alignment.centerLeft,
          width: double.infinity,
          margin: const EdgeInsets.only(top: 15, left: 30, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '이메일 주소',
                style: TextStyle(
                  fontSize: 11,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0.92,
                  color: Color.fromARGB(0xFF, 0xBE, 0xBE, 0xBE),
                ),
              ),
              TextField(
                style: TextStyle(
                  color: Color.fromARGB(0xFF, 0x26, 0x26, 0x28),
                  fontSize: 16,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0,
                ),
                onChanged: (mail) {
                  setState((){
                    _mail = mail;
                    _mailValid(_mail);
                  });
                  _next();
                },
                onSubmitted: (mail) {
                  setState((){
                    _mail = mail;
                    _mailValid(_mail);
                  });
                  _next();
                  FocusScope.of(context).requestFocus(mailNode);
                },
                keyboardType: TextInputType.emailAddress,
                cursorColor: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                controller: mailController,
                maxLines: 1,
                maxLength: 30,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 2,
                    color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                  )),
                ),
              ),
            ],
          ),
        ),
          Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 15, left: 30, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '비밀번호',
                style: TextStyle(
                  fontSize: 11,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0.92,
                  color: Color.fromARGB(0xFF, 0xBE, 0xBE, 0xBE),
                ),
                textAlign: TextAlign.start,
              ),
              TextField(
                style: TextStyle(
                  color: Color.fromARGB(0xFF, 0x26, 0x26, 0x28),
                  fontSize: 16,
                  fontFamily: 'Nanum Gothic',
                  letterSpacing: 0,
                ),
                onChanged: (pw) {
                  setState((){
                    _pw = pw;
                    _pwValid(_pw);
                  });
                  _next();
                },
                onSubmitted: (pw) {
                  setState((){
                    _pw = pw;
                    _pwValid(_pw);
                  });
                  _next();
                  FocusScope.of(context).requestFocus(pwNode);
                },
                obscureText: true,
                cursorColor: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                controller: pwController,
                maxLines: 1,
                maxLength: 20,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 2,
                    color: Color.fromARGB(0xFF, 0xFC, 0x75, 0x69),
                  )),
                ),
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        ],
      ),  
      bottomSheet: Container(
        width: double.infinity,
        height: 50,
        child: FlatButton(
          child: Text(
            '이메일로 가입하기',
          ),
          color: next ? Color.fromARGB(0xFF, 0xFC, 0x75, 0x69) : Color.fromARGB(0xFF, 0xBE, 0xBE, 0xBE),
          textColor: Color.fromARGB(0xFF, 0xFF, 0xFF, 0xFF),
          onPressed: () {
            if(next){
              _isExisted();
              FocusScope.of(context).requestFocus(FocusNode());
              // Navigator.pop(context);
              // Navigator.pop(context);
            }
          },
        ),
      ),
    );
  }

  String getName() {
    return _name;
  }

  String getMail() {
    return _mail;
  }

  String getPW() {
    return _pw;
  }

  void setName(String name) {
    setState(() {
      _name = name;
    });
  }

  void setMail(String mail) {
    setState(() {
      _mail = mail;
    });
  }

  void setPW(String pw) {
    setState(() {
      _pw = pw;
    });
  }

  void _nameValid(String name){
    setState((){
      if(name.length > 5){
        isNameValid = true;
      }
      else
        isNameValid = false;
    });
  }

  void _mailValid(String mail){
    setState((){
      if(mail.length > 5){
        isMailValid = true;
      }
      else {
        isMailValid = false;
      }
    });
  }

  void _pwValid(String pw){
    setState((){
      if(pw.length > 5){
        isPWValid = true;
      }
      else {
        isPWValid = false;
      }
    });
  }

  void _isExisted(){
    Register info;
    info.email = _mail;
    info.name = _name;
    info.pw = _pw;

    Future<Register> _register = _registerAccount();
    if(_register == null) {
      print("existed");
      print(_register);
    }
    else {
      print("not existed");
      print(_register);
    }
  }

  void _next(){
    setState((){
      if(isNameValid & isMailValid & isPWValid){
        next = true;
      }
      else{
        next = false;
      }
    });
  }
}